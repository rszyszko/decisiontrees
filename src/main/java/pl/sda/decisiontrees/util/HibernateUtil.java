package pl.sda.decisiontrees.util;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import pl.sda.decisiontrees.entity.Tree;

import java.util.HashMap;
import java.util.Map;

public class HibernateUtil {
    private static StandardServiceRegistry serviceRegistry;
    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {

        if (sessionFactory == null) {
            try {
                StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();

                Map<String, String> settings = new HashMap<>();

                settings.put(Environment.DRIVER, "org.postgres.Driver");
                settings.put(Environment.URL, "jdbc:postgresql://medsimulation.home.pl");
                settings.put(Environment.USER, "11280161_sdatree");
                settings.put(Environment.PASS, "RafalPiotr123");
                settings.put(Environment.DIALECT, "org.hibernate.dialect.PostgreSQL9Dialect");
                settings.put(Environment.SHOW_SQL, "true");
                //settings.put(Environment.HBM2DDL_AUTO, "update");

                registryBuilder.applySettings(settings);
                serviceRegistry = registryBuilder.build();

                MetadataSources metadataSources = new MetadataSources(serviceRegistry);
                metadataSources.addAnnotatedClass(Tree.class);

                Metadata metadata = metadataSources.getMetadataBuilder().build();


            } catch (Exception e) {
                e.printStackTrace();
                if (serviceRegistry != null) {
                    StandardServiceRegistryBuilder.destroy(serviceRegistry);
                }
            }
        }
        return sessionFactory;
    }

}
