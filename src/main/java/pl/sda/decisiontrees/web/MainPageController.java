package pl.sda.decisiontrees.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainPageController {
    @GetMapping(value = "/main")
    public String mainPage() {
        return "main";
    }
}
